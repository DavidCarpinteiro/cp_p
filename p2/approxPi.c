#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdint.h>
#include <memory.h>
/*#include "approxPi.h"*/

// Compile gcc approxPi.c -o approxPi -lpthread -Werror

//int simulate_sequential(int n_simulations); /* runs the simulation and returns the result */

unsigned int gettid() {
    pthread_t ptid = pthread_self();
    unsigned int threadId = 0;

    memcpy(&threadId, &ptid, sizeof(threadId));

    return threadId;
}

double rand_double(unsigned int* seed) {
    double value = rand_r(seed) / (double) RAND_MAX;
    return value;
}

int simulate_sequential(int n_simulations, unsigned int* seed) {
    int points_in_circle = 0;
    double x, y;

    for (int i = 0; i < n_simulations; i++) {
        x = rand_double(seed);
        y = rand_double(seed);

        if ((x * x + y * y) <= 1.0)
            points_in_circle++;
    }

    return points_in_circle;
}

void *simulate_parallel(void *n_sim) {
    int n_sim_value = *(int *) n_sim;

    unsigned int curr_time = (unsigned int)time(NULL);
    unsigned int seed = curr_time ^ gettid();
    srand(seed);

    int points_in_circle = simulate_sequential(n_sim_value, &seed);

    int *result = malloc(sizeof points_in_circle);//result is a int pointer, pointing to empty memory
    *result = points_in_circle; //the value that result is pointing to is equal to ...
    //result = &points_in_circle;
    //like this the address in result is an local address

    return (void *) result;
}

double run_sequential(int n_sim) {
    return 0;
}

double run_parallel(int n_sim, int n_threads) {
    int n_sim_per = n_sim / n_threads;
    int left_over = n_sim % n_threads;

    int result = 0;
    pthread_t threads[n_threads];
//    unsigned int seeds[n_threads];
//    struct parameters {
//        int n_sim_per;
//        unsigned
//    };

    for(int i = 0; i < n_threads; i++) {

        if (i == n_threads - 1)
            n_sim_per += left_over;

        pthread_t tid;
        pthread_create(&tid, NULL, simulate_parallel, &n_sim_per);
        threads[i] = tid;

        //or
        //pthread_t *t = malloc(sizeof(*t)); pointer always need to be initialized
        //pthread_create(t,...)
        //free(t) end
    }

    for(int i = 0; i < n_threads; i++) {
        void *status;
        pthread_t tid = threads[i];

        pthread_join(tid, &status);

        result += *(int *) status;

        free(status);
    }

    double pi_estimation = (result / (double) n_sim) * 4.0;

    return pi_estimation;
}

int main(int argc, char **argv) {
    double pi_estimation = 0;
    int n_sim, n_threads;

    if (argc == 2) {
        n_sim = atoi(argv[1]);
        pi_estimation = run_sequential(n_sim);
    } else if (argc == 3) {
        n_sim = atoi(argv[1]);
        n_threads = atoi(argv[2]);
        pi_estimation = run_parallel(n_sim, n_threads);
    }

    printf("Pi estimation: %lf\n", pi_estimation);

    return 0;
}


