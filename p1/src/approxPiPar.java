public class approxPiPar {

    private static final String TOTAL_POINTS = "Total Number of points: %d\n",
            POINTS_CIRCLE = "Points within circle: %d\n", PI_ESTIMATION = "Pi estimation: %f\n";

    private static final int DEFAULT_TREADS = 4;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        final int n_simulations = Integer.valueOf(args[0]);
        final int n_threads = args.length > 1 ? Integer.valueOf(args[1]) : DEFAULT_TREADS;

        int n_sim_per_thread = n_simulations / n_threads;
        int left_over = n_simulations % n_threads;

        Thread[] threads = new Thread[n_threads];
        Simulation[] simulations = new Simulation[n_threads];

        //Start Threads
        for (int i = 0; i < n_threads; i++) {

            if (i == n_threads - 1)
                n_sim_per_thread += left_over;

            Simulation simulation = new Simulation(n_sim_per_thread);
            Thread tmp = new Thread(simulation);

            tmp.start();

            threads[i] = tmp;
            simulations[i] = simulation;
        }


        //Join and Get Values
        int points_in_circle = 0;
        int simulation_ran = 0;

        for (int i = 0; i < n_threads; i++) {
            try {
                threads[i].join();
                points_in_circle += simulations[i].getPointsInCircle();
                simulation_ran += simulations[i].getNumberSimulations();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        final double pi_estimation = (points_in_circle / (double) simulation_ran) * 4;

        System.out.printf(TOTAL_POINTS + POINTS_CIRCLE + PI_ESTIMATION,
                simulation_ran, points_in_circle, pi_estimation);

        System.out.printf("Elapsed: %d ms\n", (System.currentTimeMillis() - start) );
    }

}