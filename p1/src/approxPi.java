public class approxPi {

    private static final String TOTAL_POINTS = "Total Number of points: %d\n",
        POINTS_CIRCLE = "Points within circle: %d\n", PI_ESTIMATION = "Pi estimation: %f\n";

    public static void main(String[] args) {

        final int n_simulations = Integer.valueOf(args[0]);
        int points_in_circle = 0;
        double pi_estimation;
        double dist;

        double point_x, point_y;
        for(int i = 0; i < n_simulations; i++){
            point_x = Math.random();
            point_y = Math.random();

            dist = point_x * point_x + point_y * point_y;
            if(dist <= 1.0)
                points_in_circle++;
        }

        pi_estimation = (points_in_circle / (double) n_simulations) * 4;

        System.out.printf(TOTAL_POINTS + POINTS_CIRCLE + PI_ESTIMATION,
                n_simulations, points_in_circle, pi_estimation);
    }
}
