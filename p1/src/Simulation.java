import java.util.Random;

public class Simulation implements Runnable {
    private int points_in_circle;
    private final int n_simulations;

    public Simulation(int n_simulations) {
        this.points_in_circle = 0;
        this.n_simulations = n_simulations;
    }

    @Override
    public void run() {
        double point_x, point_y;
        Random rd = new Random();

        for(int i = 0; i < n_simulations; i++){
            point_x = rd.nextDouble();
            point_y = rd.nextDouble();

            boolean in = point_x * point_x + point_y * point_y <= 1.0;
            if(in)
                points_in_circle++;
        }
    }

    public int getPointsInCircle() {
        return points_in_circle;
    }

    public int getNumberSimulations() {
        return n_simulations;
    }
}
